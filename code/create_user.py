#!python

import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "geopagos.settings")
django.setup()

from geopagos.models import CustomUser

user = CustomUser.objects.create_user(
    username='geopagos',
    password='geopagos',
    email="geopagos@geopagos.com",
    is_superuser=True,
    is_staff=True,
)
user.save()
print("New user id is {}".format(user.id))
