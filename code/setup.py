from setuptools import setup

setup(
    name='geopagos',
    version="1.0.0",
    scripts=[
        "manage.py",
        "create_user.py"
    ],
    packages=[
        'geopagos',
        'geopagos.controllers',
        'geopagos.migrations',
        'geopagos.tests'
    ],
    install_requires=[
        "django<1.12",
        "djangorestframework<3.7",
        "python-dateutil",
    ]
)
