from hashlib import sha256
from django.db import models
from django.db.models import Avg, Sum, F
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    """Extension of Django's user model with an extra address filed."""

    address = models.CharField(max_length=50, blank=False, null=False)
    email = models.EmailField(primary_key=True)
    id = models.CharField(
        max_length=64,
        null=True,
        blank=True,
        unique=True,
        default=sha256(str(email).encode('utf8')).hexdigest()
    )


class Payment(models.Model):
    """The amount of mony payed by a user on a given date."""

    user_email = models.ForeignKey(
        CustomUser,
        related_name='payments',
        on_delete=models.CASCADE
    )
    amount = models.FloatField()
    date = models.DateTimeField()

    class Meta:
        permissions = (
            ('view_task', 'View task'),
        )

    def __str__(self):
        return "{} / {} / {}".format(
            self.user_email.username,
            self.amount,
            self.date
        )


class PaymentsSummary():
    """Container for the payments summary info between dates of an user."""

    def __init__(self, user, initial_date, final_date):

        self.number = Payment.objects\
            .filter(user_email=user)\
            .filter(date__date__gte=initial_date)\
            .filter(date__date__lte=final_date)\
            .count()
        self.total = Payment.objects\
            .filter(user_email=user)\
            .filter(date__date__gte=initial_date)\
            .filter(date__date__lte=final_date)\
            .aggregate(Sum('amount'))["amount__sum"]
        self.average = Payment.objects\
            .filter(user_email=user)\
            .filter(date__date__gte=initial_date)\
            .filter(date__date__lte=final_date)\
            .aggregate(Avg('amount'))["amount__avg"]
