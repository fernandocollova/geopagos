from geopagos.models import CustomUser, Payment
from django.test import TestCase, Client

from geopagos.controllers.views import PaymentSummaryView, PaymentView


class TestCreateEndpoint(TestCase):

    def setUp(self):

        self.password = 'test'
        self.user = CustomUser.objects.create_user(
            username='test',
            password=self.password,
            email="ejemplo@geopagos.com",
            is_superuser=True,
            is_staff=True
        )
        self.user.save()

        self.client = Client()
        self.client.login(
            username=self.user.username,
            password=self.password
        )

        self.ok_payment_data = {
            'user_email': 'ejemplo@geopagos.com',
            'amount': 123.45,
            'date': '2017-10-15 11:35'
        }
        self.broken_payment_data = {
            'user_email': 'not@working.com',
            'amount': 123.45,
            'date': '2017-10-15 11:35'
        }

    def tearDown(self):
        self.user.delete()
        for payment in Payment.objects.all():
            payment.delete()

    def test_payment_is_created(self):

        response = self.client.post('/payments/', self.ok_payment_data)
        self.assertEqual(response.status_code, 201)

    def test_wrong_email_fails(self):

        response = self.client.post('/payments/', self.broken_payment_data)
        self.assertEqual(response.status_code, 400)


class TestLogin(TestCase):

    def setUp(self):
        self.client = Client()
        self.ok_payment_data = {
            'user_email': 'ejemplo@geopagos.com',
            'amount': 123.45,
            'date': '2017-10-15 11:35'
        }

    def test_not_logged_post_fails(self):

        response = self.client.post('/payments/', self.ok_payment_data)
        self.assertEqual(response.status_code, 403)


class TestSummaryEndpoint(TestCase):

    def setUp(self):

        self.password = 'test'
        self.user = CustomUser.objects.create_user(
            username='test',
            password=self.password,
            email="ejemplo@geopagos.com",
            is_superuser=True,
            is_staff=True
        )
        self.user.save()

        self.client = Client()
        self.client.login(
            username=self.user.username,
            password=self.password
        )

        payment_1 = {
            'user_email': 'ejemplo@geopagos.com',
            'amount': 1,
            'date': '2017-10-15 11:35'
        }
        payment_2 = {
            'user_email': 'ejemplo@geopagos.com',
            'amount': 2,
            'date': '2017-10-16 11:35'
        }
        payment_3 = {
            'user_email': 'ejemplo@geopagos.com',
            'amount': 3,
            'date': '2017-10-17 11:35'
        }
        payment_4 = {
            'user_email': 'ejemplo@geopagos.com',
            'amount': 15000,
            'date': '2017-10-18 11:35'
        }
        self.payments = [payment_1, payment_2, payment_3, payment_4]
        self.initial_date = "2017-10-14"
        self.final_date = "2017-10-17"

    def tearDown(self):

        self.user.delete()
        for payment in Payment.objects.all():
            payment.delete()

    def test_summary(self):

        for payment in self.payments:
            response = self.client.post('/payments/', payment)
            self.assertEqual(response.status_code, 201)

        response = self.client.get(
            "/payments/summary/{}/{}/{}/".format(
                self.user.id,
                self.initial_date,
                self.final_date
            )
        )

        self.assertEqual(
            response.content, b'{"number":3,"total":6.0,"average":2.0}'
        )
