"""geopagos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from geopagos.controllers.views import (
    PaymentSummaryView,
    PaymentView,
    PaymentListView,
)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^payments/$', PaymentView.as_view()),
    url(
        r'^payments/list/'
        + r'(?P<user_id>.+)/'
        + r'(?P<initial_date>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})/'
        + r'(?P<final_date>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})/$',
        PaymentListView.as_view()
    ),
    url(
        r'^payments/summary/'
        + r'(?P<user_id>.+)/'
        + r'(?P<initial_date>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})/'
        + r'(?P<final_date>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})/$',
        PaymentSummaryView.as_view()
    ),
]
