from django.contrib import admin
from geopagos.models import CustomUser, Payment


class CustomUserAdmin(admin.ModelAdmin):

    def has_delete_permission(self, request, obj=None):
        return False


class PaymentAdmin(admin.ModelAdmin):
    readonly_fields = ('user_email', 'amount', 'date')

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Payment, PaymentAdmin)
