from django.apps import AppConfig

class GeoPagosConfig(AppConfig):
    name = 'geopagos'
    verbose_name = "geopagos"
