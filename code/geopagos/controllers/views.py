from dateutil import parser
from dateutil.tz import tzlocal
from rest_framework import generics, status, permissions
from rest_framework.response import Response

from geopagos.controllers.serializers import (
    PaymentSerializer,
    PaymentSummarySerializer
)
from geopagos.models import (
    CustomUser,
    Payment,
    PaymentsSummary
)


class PaymentView(generics.CreateAPIView):

    serializer_class = PaymentSerializer
    permission_classes = (permissions.IsAuthenticated,)


class PaymentListView(generics.ListAPIView):
    """List a user's payment between two dates."""

    model = Payment
    serializer_class = PaymentSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):

        user = CustomUser.objects.filter(id=self.kwargs['user'])
        initial_date = parser.parse(self.kwargs['initial_date']).date()
        final_date = parser.parse(self.kwargs['final_date']).date()

        return Payment.objects\
            .filter(user=user)\
            .filter(date__gte=initial_date)\
            .filter(date__lte=final_date)


class PaymentSummaryView(generics.RetrieveAPIView):

    serializer_class = PaymentSummarySerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, user, initial_date, final_date):
        """Return the payments summary object."""

        return PaymentsSummary(user, initial_date, final_date)

    def get(self, request, user_id, initial_date, final_date):

        user = CustomUser.objects.get(id=user_id)
        initial_date = parser.parse(initial_date).date()
        final_date = parser.parse(final_date).date()

        summary = self.get_object(
            user,
            initial_date,
            final_date
        )

        serializer = PaymentSummarySerializer(summary)
        return Response(serializer.data, status=status.HTTP_200_OK)
