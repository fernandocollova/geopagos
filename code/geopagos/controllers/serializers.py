from geopagos.models import Payment
from rest_framework import serializers


class PaymentSerializer(serializers.ModelSerializer):
    """Store the Answer of a NumberValuedQuestion."""

    class Meta:
        model = Payment
        fields = ('id', 'user_email', 'amount', 'date')


class PaymentSummarySerializer(serializers.Serializer):

    number = serializers.IntegerField(read_only=True)
    total = serializers.FloatField(read_only=True)
    average = serializers.FloatField(read_only=True)
