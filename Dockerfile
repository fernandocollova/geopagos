FROM alpine:3.7
COPY code /code
RUN  \
    apk update && \
    apk upgrade && \
    apk add python3 py3-psycopg2 && \
    python3 -m pip install --upgrade --no-cache-dir pip && \
    python3 -m pip install --upgrade --no-cache-dir /code && \
    rm -fr /code
