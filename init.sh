#!/bin/sh

python -m compose build prod
python -m compose up -d db
echo "Waiting 5 seconds for Postrgres to be ready"
sleep 5
python -m compose run --rm --entrypoint="manage.py migrate" prod
python -m compose run --rm --entrypoint="create_user.py" prod
